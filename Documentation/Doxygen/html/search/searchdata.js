var indexSectionsWithContent =
{
  0: "acdfghilmnoprsuvx",
  1: "adfghilmopruv",
  2: "gx",
  3: "acdghilmoprsv",
  4: "s",
  5: "cmnpr",
  6: "ls",
  7: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "properties",
  6: "events",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Properties",
  6: "Events",
  7: "Pages"
};

