var searchData=
[
  ['saveleaderboard_76',['SaveLeaderboard',['../interface_gold_miner_1_1_repository_1_1_i_permanent_storage_repository.html#ad5d0b02caccd4335f27a899aea2b18e1',1,'GoldMiner.Repository.IPermanentStorageRepository.SaveLeaderboard()'],['../class_gold_miner_1_1_repository_1_1_permanent_storage_repository.html#ae464cb3362cbeb3cabfaba977386bde3',1,'GoldMiner.Repository.PermanentStorageRepository.SaveLeaderboard()']]],
  ['scoreupdated_77',['ScoreUpdated',['../class_gold_miner_1_1_model_1_1_player.html#af29721445ce1a5fade3068b064240032',1,'GoldMiner::Model::Player']]],
  ['setpropertyvalue_78',['SetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.SetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, object value, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.SetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, object value, System.Globalization.CultureInfo culture)']]],
  ['shoothook_79',['ShootHook',['../class_gold_miner_1_1_logic_1_1_logic.html#ac05e09b815adcf843be3705e0d4d7459',1,'GoldMiner::Logic::Logic']]],
  ['stonesizetype_80',['StoneSizeType',['../namespace_gold_miner_1_1_model_1_1_helper.html#ab8d5b1b912a63a89d427920932b35145',1,'GoldMiner::Model::Helper']]]
];
