var searchData=
[
  ['leaderboardpage_47',['LeaderboardPage',['../class_gold_miner_1_1_view_1_1_views_1_1_leaderboard_page.html',1,'GoldMiner::View::Views']]],
  ['leaderboardviewmodel_48',['LeaderboardViewModel',['../class_gold_miner_1_1_view_1_1_view_models_1_1_leaderboard_view_model.html',1,'GoldMiner::View::ViewModels']]],
  ['levelchanged_49',['LevelChanged',['../class_gold_miner_1_1_model_1_1_player.html#a80184a8a6984d3aaab05ad7f48694f94',1,'GoldMiner::Model::Player']]],
  ['levellimitchanged_50',['LevelLimitChanged',['../class_gold_miner_1_1_model_1_1_player.html#a695b41787aa9408cfc931657c2aab85f',1,'GoldMiner::Model::Player']]],
  ['loadleaderboard_51',['LoadLeaderboard',['../interface_gold_miner_1_1_repository_1_1_i_permanent_storage_repository.html#a55ed2a24b75e331abfc280639c995790',1,'GoldMiner.Repository.IPermanentStorageRepository.LoadLeaderboard()'],['../class_gold_miner_1_1_repository_1_1_permanent_storage_repository.html#ac1de4416e67825f52bc564b721abdcce',1,'GoldMiner.Repository.PermanentStorageRepository.LoadLeaderboard()']]],
  ['logic_52',['Logic',['../class_gold_miner_1_1_logic_1_1_logic.html',1,'GoldMiner.Logic.Logic'],['../class_gold_miner_1_1_logic_1_1_logic.html#ae4c90b9ce01c6a893cbb096344499c36',1,'GoldMiner.Logic.Logic.Logic()']]],
  ['logictest_53',['LogicTest',['../class_gold_miner_1_1_logic_1_1_test_1_1_logic_test.html',1,'GoldMiner::Logic::Test']]],
  ['license_54',['LICENSE',['../md__c_1__users__szombathelyi_p_xC3_xA9ter__desktop_prog4_oenik_prog4_2020_1_o9q4m4_g6dkbq__gold_78ace7b1f5afac45280fcb0c3ffe4bf9.html',1,'']]]
];
