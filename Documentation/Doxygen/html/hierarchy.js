var hierarchy =
[
    [ "GoldMiner.Repository.Account", "class_gold_miner_1_1_repository_1_1_account.html", null ],
    [ "Application", null, [
      [ "GoldMiner.View.App", "class_gold_miner_1_1_view_1_1_app.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "GoldMiner.View.Renderer.GameControl", "class_gold_miner_1_1_view_1_1_renderer_1_1_game_control.html", null ]
    ] ],
    [ "GoldMiner.Model.Base.GameItem", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html", [
      [ "GoldMiner.Model.Base.Mineral", "class_gold_miner_1_1_model_1_1_base_1_1_mineral.html", [
        [ "GoldMiner.Model.Diamond", "class_gold_miner_1_1_model_1_1_diamond.html", null ],
        [ "GoldMiner.Model.Gold", "class_gold_miner_1_1_model_1_1_gold.html", null ],
        [ "GoldMiner.Model.Oil", "class_gold_miner_1_1_model_1_1_oil.html", null ],
        [ "GoldMiner.Model.Plastic", "class_gold_miner_1_1_model_1_1_plastic.html", null ],
        [ "GoldMiner.Model.Rock", "class_gold_miner_1_1_model_1_1_rock.html", null ]
      ] ],
      [ "GoldMiner.Model.Hook", "class_gold_miner_1_1_model_1_1_hook.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "GoldMiner.View.Views.FinishPage", "class_gold_miner_1_1_view_1_1_views_1_1_finish_page.html", null ],
      [ "GoldMiner.View.Views.GamePage", "class_gold_miner_1_1_view_1_1_views_1_1_game_page.html", null ],
      [ "GoldMiner.View.Views.HelpPage", "class_gold_miner_1_1_view_1_1_views_1_1_help_page.html", null ],
      [ "GoldMiner.View.Views.HomePage", "class_gold_miner_1_1_view_1_1_views_1_1_home_page.html", null ],
      [ "GoldMiner.View.Views.LeaderboardPage", "class_gold_miner_1_1_view_1_1_views_1_1_leaderboard_page.html", null ],
      [ "GoldMiner.View.Views.UserForm", "class_gold_miner_1_1_view_1_1_views_1_1_user_form.html", null ]
    ] ],
    [ "INavigationService", null, [
      [ "GoldMiner.View.Helper.IFrameNavigationService", "interface_gold_miner_1_1_view_1_1_helper_1_1_i_frame_navigation_service.html", [
        [ "GoldMiner.View.Helper.FrameNavigationService", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html", null ]
      ] ]
    ] ],
    [ "INotifyPropertyChanged", null, [
      [ "GoldMiner.View.Helper.FrameNavigationService", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "GoldMiner.Repository.IPermanentStorageRepository", "interface_gold_miner_1_1_repository_1_1_i_permanent_storage_repository.html", [
      [ "GoldMiner.Repository.PermanentStorageRepository", "class_gold_miner_1_1_repository_1_1_permanent_storage_repository.html", null ]
    ] ],
    [ "GoldMiner.Repository.IPlayerRepository", "interface_gold_miner_1_1_repository_1_1_i_player_repository.html", [
      [ "GoldMiner.Repository.PlayerRepository", "class_gold_miner_1_1_repository_1_1_player_repository.html", null ]
    ] ],
    [ "GoldMiner.Logic.Logic", "class_gold_miner_1_1_logic_1_1_logic.html", null ],
    [ "GoldMiner.Logic.Test.LogicTest", "class_gold_miner_1_1_logic_1_1_test_1_1_logic_test.html", null ],
    [ "GoldMiner.Model.Model", "class_gold_miner_1_1_model_1_1_model.html", null ],
    [ "Page", null, [
      [ "GoldMiner.View.Views.FinishPage", "class_gold_miner_1_1_view_1_1_views_1_1_finish_page.html", null ],
      [ "GoldMiner.View.Views.GamePage", "class_gold_miner_1_1_view_1_1_views_1_1_game_page.html", null ],
      [ "GoldMiner.View.Views.HelpPage", "class_gold_miner_1_1_view_1_1_views_1_1_help_page.html", null ],
      [ "GoldMiner.View.Views.HomePage", "class_gold_miner_1_1_view_1_1_views_1_1_home_page.html", null ],
      [ "GoldMiner.View.Views.LeaderboardPage", "class_gold_miner_1_1_view_1_1_views_1_1_leaderboard_page.html", null ],
      [ "GoldMiner.View.Views.UserForm", "class_gold_miner_1_1_view_1_1_views_1_1_user_form.html", null ],
      [ "GoldMiner.View.Views.UserFormPage", "class_gold_miner_1_1_view_1_1_views_1_1_user_form_page.html", null ]
    ] ],
    [ "GoldMiner.Model.Player", "class_gold_miner_1_1_model_1_1_player.html", null ],
    [ "ViewModelBase", null, [
      [ "GoldMiner.View.ViewModel.MainViewModel", "class_gold_miner_1_1_view_1_1_view_model_1_1_main_view_model.html", null ],
      [ "GoldMiner.View.ViewModels.FinishPageViewModel", "class_gold_miner_1_1_view_1_1_view_models_1_1_finish_page_view_model.html", null ],
      [ "GoldMiner.View.ViewModels.GamePageViewModel", "class_gold_miner_1_1_view_1_1_view_models_1_1_game_page_view_model.html", null ],
      [ "GoldMiner.View.ViewModels.HelpPageViewModel", "class_gold_miner_1_1_view_1_1_view_models_1_1_help_page_view_model.html", null ],
      [ "GoldMiner.View.ViewModels.HomePageViewModel", "class_gold_miner_1_1_view_1_1_view_models_1_1_home_page_view_model.html", null ],
      [ "GoldMiner.View.ViewModels.LeaderboardViewModel", "class_gold_miner_1_1_view_1_1_view_models_1_1_leaderboard_view_model.html", null ],
      [ "GoldMiner.View.ViewModels.MainWindowViewModel", "class_gold_miner_1_1_view_1_1_view_models_1_1_main_window_view_model.html", null ],
      [ "GoldMiner.View.ViewModels.UserFormPageViewModel", "class_gold_miner_1_1_view_1_1_view_models_1_1_user_form_page_view_model.html", null ]
    ] ],
    [ "GoldMiner.View.ViewModelLocator", "class_gold_miner_1_1_view_1_1_view_model_locator.html", null ],
    [ "GoldMiner.View.ViewModel.ViewModelLocator", "class_gold_miner_1_1_view_1_1_view_model_1_1_view_model_locator.html", null ],
    [ "Window", null, [
      [ "GoldMiner.View.MainWindow", "class_gold_miner_1_1_view_1_1_main_window.html", null ]
    ] ]
];