var class_gold_miner_1_1_view_1_1_view_models_1_1_game_page_view_model =
[
    [ "GamePageViewModel", "class_gold_miner_1_1_view_1_1_view_models_1_1_game_page_view_model.html#a7ea167e24b75b011e1d794a403401e9f", null ],
    [ "NavigateToFinishPage", "class_gold_miner_1_1_view_1_1_view_models_1_1_game_page_view_model.html#ae436cb81f530f739a9cdbaaf988dcf23", null ],
    [ "BackCommand", "class_gold_miner_1_1_view_1_1_view_models_1_1_game_page_view_model.html#aef51e032183de3c526016db6ef908405", null ],
    [ "PageLoadedCommand", "class_gold_miner_1_1_view_1_1_view_models_1_1_game_page_view_model.html#ad18de5092976a907b7a6e685f5261e5c", null ],
    [ "LevelLimit", "class_gold_miner_1_1_view_1_1_view_models_1_1_game_page_view_model.html#a43ddb60c5518a65964ed989cd9f9efb3", null ],
    [ "UserLevel", "class_gold_miner_1_1_view_1_1_view_models_1_1_game_page_view_model.html#ad6b469c7f57202f42ac8f5dd055b0615", null ],
    [ "UserScore", "class_gold_miner_1_1_view_1_1_view_models_1_1_game_page_view_model.html#aa0bc33a0c79a999293e63ccafd3467a8", null ]
];