var class_gold_miner_1_1_model_1_1_player =
[
    [ "Player", "class_gold_miner_1_1_model_1_1_player.html#a86c13bf9dd5a7573350f2746d959e4e6", null ],
    [ "GameEndTime", "class_gold_miner_1_1_model_1_1_player.html#a084eba3a438406f0ea9293073ba9989d", null ],
    [ "GameStartTime", "class_gold_miner_1_1_model_1_1_player.html#afd2a3fb523a54ba242a329c82cc20798", null ],
    [ "Level", "class_gold_miner_1_1_model_1_1_player.html#a95510b0e205bca617707b377693b3249", null ],
    [ "LevelLimit", "class_gold_miner_1_1_model_1_1_player.html#a70967b11c5264f188451d40b26399d01", null ],
    [ "Name", "class_gold_miner_1_1_model_1_1_player.html#a3fad711c193431bbc7214595c08f7e3d", null ],
    [ "Score", "class_gold_miner_1_1_model_1_1_player.html#a77c9f4350805dc66573b9b0d9276340c", null ],
    [ "LevelChanged", "class_gold_miner_1_1_model_1_1_player.html#a80184a8a6984d3aaab05ad7f48694f94", null ],
    [ "LevelLimitChanged", "class_gold_miner_1_1_model_1_1_player.html#a695b41787aa9408cfc931657c2aab85f", null ],
    [ "ScoreUpdated", "class_gold_miner_1_1_model_1_1_player.html#af29721445ce1a5fade3068b064240032", null ]
];