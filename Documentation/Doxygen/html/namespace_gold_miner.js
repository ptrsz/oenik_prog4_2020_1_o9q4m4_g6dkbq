var namespace_gold_miner =
[
    [ "Logic", "namespace_gold_miner_1_1_logic.html", "namespace_gold_miner_1_1_logic" ],
    [ "Model", "namespace_gold_miner_1_1_model.html", "namespace_gold_miner_1_1_model" ],
    [ "Repository", "namespace_gold_miner_1_1_repository.html", "namespace_gold_miner_1_1_repository" ],
    [ "View", "namespace_gold_miner_1_1_view.html", "namespace_gold_miner_1_1_view" ]
];