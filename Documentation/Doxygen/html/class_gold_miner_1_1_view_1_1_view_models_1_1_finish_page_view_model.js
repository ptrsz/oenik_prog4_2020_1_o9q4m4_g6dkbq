var class_gold_miner_1_1_view_1_1_view_models_1_1_finish_page_view_model =
[
    [ "FinishPageViewModel", "class_gold_miner_1_1_view_1_1_view_models_1_1_finish_page_view_model.html#a9796ab7de9a6a08b15c029859fd09e0b", null ],
    [ "BackCommand", "class_gold_miner_1_1_view_1_1_view_models_1_1_finish_page_view_model.html#a529cf16b55a646e15d23ee133afbbeab", null ],
    [ "PageLoadedCommand", "class_gold_miner_1_1_view_1_1_view_models_1_1_finish_page_view_model.html#ad3627c05a01520d0723ccaeb54888a6d", null ],
    [ "CurrentPlayer", "class_gold_miner_1_1_view_1_1_view_models_1_1_finish_page_view_model.html#a562bc36c7418797dbbb53304cad432c2", null ]
];