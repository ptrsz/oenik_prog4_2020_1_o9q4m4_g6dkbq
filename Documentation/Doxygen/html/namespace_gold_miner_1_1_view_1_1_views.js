var namespace_gold_miner_1_1_view_1_1_views =
[
    [ "FinishPage", "class_gold_miner_1_1_view_1_1_views_1_1_finish_page.html", "class_gold_miner_1_1_view_1_1_views_1_1_finish_page" ],
    [ "GamePage", "class_gold_miner_1_1_view_1_1_views_1_1_game_page.html", "class_gold_miner_1_1_view_1_1_views_1_1_game_page" ],
    [ "HelpPage", "class_gold_miner_1_1_view_1_1_views_1_1_help_page.html", "class_gold_miner_1_1_view_1_1_views_1_1_help_page" ],
    [ "HomePage", "class_gold_miner_1_1_view_1_1_views_1_1_home_page.html", "class_gold_miner_1_1_view_1_1_views_1_1_home_page" ],
    [ "LeaderboardPage", "class_gold_miner_1_1_view_1_1_views_1_1_leaderboard_page.html", "class_gold_miner_1_1_view_1_1_views_1_1_leaderboard_page" ],
    [ "UserForm", "class_gold_miner_1_1_view_1_1_views_1_1_user_form.html", "class_gold_miner_1_1_view_1_1_views_1_1_user_form" ],
    [ "UserFormPage", "class_gold_miner_1_1_view_1_1_views_1_1_user_form_page.html", null ]
];