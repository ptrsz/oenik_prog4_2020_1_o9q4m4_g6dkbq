var class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service =
[
    [ "FrameNavigationService", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html#adccebd529b65cba17da5c9e41b538b8b", null ],
    [ "Configure", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html#a5e817af698d5b2c8c036eb814fef8041", null ],
    [ "GoBack", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html#a3fc1be024a2538b0e05dc31e96a97bd9", null ],
    [ "NavigateTo", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html#a6eb4f349bd111bf153819647da2e7ef1", null ],
    [ "NavigateTo", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html#a7ad7b67fe28caae3e16abb817229e95f", null ],
    [ "OnPropertyChanged", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html#a71c185d5d95758945bea9ab0aee10aa8", null ],
    [ "CurrentPageKey", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html#a41df46abd446d891a0c7d77fd26d3f21", null ],
    [ "Parameter", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html#ab869cd29db1bfa84c2a2aa50d1eb5bda", null ],
    [ "PropertyChanged", "class_gold_miner_1_1_view_1_1_helper_1_1_frame_navigation_service.html#aae1e8d2bdcf373fbc400abc22c8557ef", null ]
];