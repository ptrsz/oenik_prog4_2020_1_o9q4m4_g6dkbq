var namespace_gold_miner_1_1_repository =
[
    [ "Account", "class_gold_miner_1_1_repository_1_1_account.html", "class_gold_miner_1_1_repository_1_1_account" ],
    [ "IPermanentStorageRepository", "interface_gold_miner_1_1_repository_1_1_i_permanent_storage_repository.html", "interface_gold_miner_1_1_repository_1_1_i_permanent_storage_repository" ],
    [ "IPlayerRepository", "interface_gold_miner_1_1_repository_1_1_i_player_repository.html", "interface_gold_miner_1_1_repository_1_1_i_player_repository" ],
    [ "PermanentStorageRepository", "class_gold_miner_1_1_repository_1_1_permanent_storage_repository.html", "class_gold_miner_1_1_repository_1_1_permanent_storage_repository" ],
    [ "PlayerRepository", "class_gold_miner_1_1_repository_1_1_player_repository.html", "class_gold_miner_1_1_repository_1_1_player_repository" ]
];