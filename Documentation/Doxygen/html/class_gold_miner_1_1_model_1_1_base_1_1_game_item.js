var class_gold_miner_1_1_model_1_1_base_1_1_game_item =
[
    [ "GameItem", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html#a66c70468ee41c79375b024b0109fac11", null ],
    [ "IsCollision", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html#afe1ddf756ec01336ccf999037c9c964b", null ],
    [ "area", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html#adeefb1a05cbb5bed84000a3c07d7f9a3", null ],
    [ "InitPosition", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html#adad40b32d5efe2e1fef7c1ca33589ec7", null ],
    [ "Position", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html#a1f14e825380ad8cd147f9c1eec29b2d5", null ],
    [ "RotDegree", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html#aa85fa3edc788dc0e38447da1ecc9ddff", null ],
    [ "IsEnabled", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html#a738cc9f1a7f8a810c941de15d7a658de", null ],
    [ "PositionAsPoint", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html#a8eda89343ca373674429ac4b83c19f35", null ],
    [ "Rad", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html#ab2811a836666f07b7f33f2bd0bf5acc0", null ],
    [ "RealArea", "class_gold_miner_1_1_model_1_1_base_1_1_game_item.html#a32869b1ff514ae8016c95267cf0ed287", null ]
];