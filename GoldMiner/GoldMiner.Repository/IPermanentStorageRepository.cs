﻿// <copyright file="IPermanentStorageRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Interface for storage repository.
    /// </summary>
    public interface IPermanentStorageRepository
    {
        /// <summary>
        /// Saves leaderboard.
        /// </summary>
        void SaveLeaderboard();

        /// <summary>
        /// Loads leaderboard.
        /// </summary>
        void LoadLeaderboard();
    }
}
