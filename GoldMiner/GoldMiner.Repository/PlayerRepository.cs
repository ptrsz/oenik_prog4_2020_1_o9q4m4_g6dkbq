﻿// <copyright file="PlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GoldMiner.Model;

    /// <summary>
    /// Player repository class.
    /// </summary>
    public class PlayerRepository : IPlayerRepository
    {
        /// <summary>
        /// Gets or sets the player list.
        /// </summary>
        public List<Player> PlayerList { get; set; } = new List<Player>();

        /// <summary>
        /// Gets or sets the current player.
        /// </summary>
        public Player CurrentPlayer { get; set; }
    }
}