﻿// <copyright file="IPlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GoldMiner.Model;

    /// <summary>
    /// Interface for player repository.
    /// </summary>
    public interface IPlayerRepository
    {
        /// <summary>
        /// Gets or sets current player.
        /// </summary>
         Player CurrentPlayer { get; set; }

        /// <summary>
        /// Gets or sets player list.
        /// </summary>
         List<Player> PlayerList { get; set; }
    }
}
