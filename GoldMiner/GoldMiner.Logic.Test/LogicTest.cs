﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Logic.Test
{
    using System.Collections.Generic;
    using System.Linq;
    using GoldMiner.Model;
    using GoldMiner.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// This class handles the tests for the Logic layer.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Logic logic;
        private Model model;

        /// <summary>
        /// Initializes Moq and model, instantiate logic.
        /// </summary>
        [SetUp]
        public void Init()
        {
            var playerRepository = new Mock<IPlayerRepository>();
            var currentPlayer = new Player();

            playerRepository.Setup(x => x.CurrentPlayer).Returns(currentPlayer);
            playerRepository.Setup(x => x.PlayerList).Returns(new List<Player> { currentPlayer });

            this.model = new Model();
            var level = "GoldMiner.Logic.Levels.one.lvl";

            this.logic = new Logic(this.model, level, currentPlayer);
        }

        /// <summary>
        /// Test if hook is rotating clockwise at start.
        /// </summary>
        [Test]
        public void IsHookRotatingClockwiseTest()
        {
            Assert.IsTrue(this.logic.IsHookRotatingClockwise);
        }

        /// <summary>
        /// Test if hook is not shooted at start.
        /// </summary>
        [Test]
        public void IsHookShootedAtStartTest()
        {
            Assert.IsFalse(this.logic.IsHookShooted);
        }

        /// <summary>
        /// Test if we shoot the hook it's actually shooted.
        /// </summary>
        [Test]
        public void HookShootTest()
        {
            Assert.IsFalse(this.logic.IsHookShooted);

            this.logic.ShootHook();

            Assert.IsTrue(this.logic.IsHookShooted);
        }

        /// <summary>
        /// Test if we shoot the hook it starts moving.
        /// </summary>
        [Test]
        public void HookIsMovingAfterShootTest()
        {
            var positionBeforeShoot = this.model.Hook.Position;

            int i = 0;
            while (i < 10)
            {
                this.logic.ShootHook();
                i++;
            }

            var positionAfterShoot = this.model.Hook.Position;

            Assert.IsTrue(positionAfterShoot.Y > positionBeforeShoot.Y);
        }

        /// <summary>
        /// Test if we clear the map it's actually cleared.
        /// </summary>
        [Test]
        public void MapIsClearedAfterClearTest()
        {
            Assert.IsTrue(this.model.GoldList.Count > 0);

            this.logic.ClearModel();

            Assert.IsTrue(this.model.GoldList.Count == 0);
        }

        /// <summary>
        /// Test if we collide with diamond we pull the hook back.
        /// </summary>
        [Test]
        public void DiamondCollisionTest()
        {
            var diamondPositionBeforeCatch = this.model.DiamondList.First().Position;

            this.model.Hook.Position = diamondPositionBeforeCatch;
            this.model.Hook.Position.Y -= 15;
            this.model.Hook.Position.X += 1;

            this.logic.ShootHook();

            int i = 0;
            while (i < 10)
            {
                this.logic.PullHook();
                i++;
            }

            var diamondPositionAfterCatchAndPull = this.model.DiamondList.First().Position;

            Assert.IsTrue(diamondPositionAfterCatchAndPull.Y < diamondPositionBeforeCatch.Y);
        }
    }
}
