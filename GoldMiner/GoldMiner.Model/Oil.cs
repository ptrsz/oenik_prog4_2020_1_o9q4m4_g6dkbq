﻿// <copyright file="Oil.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model
{
    using System;
    using System.Collections.Generic;
    using System.Numerics;
    using System.Text;
    using GoldMiner.Model.Base;
    using GoldMiner.Model.Helper;

    /// <summary>
    /// Oil game item class.
    /// </summary>
    public class Oil : Mineral
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Oil"/> class.
        /// </summary>
        /// <param name="position">position.</param>
        /// <param name="stoneSizeType">stone size type</param>
        public Oil(Vector2 position, StoneSizeType stoneSizeType)
            : base(position, stoneSizeType)
        {
            switch (stoneSizeType)
            {
                case StoneSizeType.Small:
                    this.BrushName = "GoldMiner.View.Images.oil.png"; // TODO
                    break;
                case StoneSizeType.Medium:
                    this.BrushName = "GoldMiner.View.Images.oil.png";
                    this.Value = 300;
                    this.Weight = 5;
                    break;
                case StoneSizeType.Large:
                    this.BrushName = "GoldMiner.View.Images.oil.png"; // TODO
                    break;
                default:
                    break;
            }
        }
    }
}
