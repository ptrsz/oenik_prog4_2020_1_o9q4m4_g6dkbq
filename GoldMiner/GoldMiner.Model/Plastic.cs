﻿// <copyright file="Plastic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model
{
    using System;
    using System.Collections.Generic;
    using System.Numerics;
    using System.Text;
    using GoldMiner.Model.Base;
    using GoldMiner.Model.Helper;

    /// <summary>
    /// Plastic game item class.
    /// </summary>
    public class Plastic : Mineral
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Plastic"/> class.
        /// </summary>
        /// <param name="position">position.</param>
        /// <param name="stoneSizeType">stone size type.</param>
        public Plastic(Vector2 position, StoneSizeType stoneSizeType)
            : base(position, stoneSizeType)
        {
            switch (stoneSizeType)
            {
                case StoneSizeType.Small:
                    this.BrushName = "GoldMiner.View.Images.plastic.png"; // TODO
                    break;
                case StoneSizeType.Medium:
                    this.BrushName = "GoldMiner.View.Images.plastic.png";
                    this.Value = 5;
                    this.Weight = 1;
                    break;
                case StoneSizeType.Large:
                    this.BrushName = "GoldMiner.View.Images.plastic.png"; // TODO
                    break;
                default:
                    break;
            }
        }
    }
}
