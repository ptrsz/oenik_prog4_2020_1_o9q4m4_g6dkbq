﻿// <copyright file="Model.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model
{
    using System;
    using System.Collections.Generic;
    using System.Numerics;
    using System.Windows;
    using GoldMiner.Model.Base;

    /// <summary>
    /// Model class for the game model.
    /// </summary>
    public class Model
    {
        public Size TileSize { get; set; }

        public Point PlayerPos;

        // ezek innentol mind GameItem leszarmazottak lesznek
        public Hook Hook;
        public List<Gold> GoldList;
        public List<Diamond> DiamondList;
        public List<Rock> RockList;
        public List<Oil> OilList;
        public List<Plastic> PlasticList;

        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class.
        /// </summary>
        public Model()
        {
            this.GoldList = new List<Gold>();
            this.DiamondList = new List<Diamond>();
            this.RockList = new List<Rock>();
            this.OilList = new List<Oil>();
            this.PlasticList = new List<Plastic>();

            this.PlayerPos = new Point(24, 0);
            this.Hook = new Hook(new Vector2(500, 60));
        }

        /// <summary>
        /// Gets list of minerals in model class.
        /// </summary>
        public List<Mineral> Minerals
        {
            get
            {
                var mineralList = new List<Mineral>();
                mineralList.AddRange(this.GoldList);
                mineralList.AddRange(this.DiamondList);
                mineralList.AddRange(this.RockList);
                mineralList.AddRange(this.OilList);
                mineralList.AddRange(this.PlasticList);

                return mineralList;
            }
        }
    }
}
