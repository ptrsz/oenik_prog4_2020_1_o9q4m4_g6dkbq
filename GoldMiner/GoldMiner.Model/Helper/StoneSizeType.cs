﻿// <copyright file="StoneSizeType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model.Helper
{
    /// <summary>
    /// Stone size enumeration.
    /// </summary>
    public enum StoneSizeType
    {
        Small,
        Medium,
        Large
    }
}
