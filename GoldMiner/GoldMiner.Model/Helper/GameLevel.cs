﻿// <copyright file="GameLevel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal enum GameLevel
    {
        Level_01,
        Level_02,
        Level_03,
    }
}
