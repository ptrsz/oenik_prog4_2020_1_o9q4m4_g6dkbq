﻿// <copyright file="Hook.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model
{
    using System;
    using System.Collections.Generic;
    using System.Numerics;
    using System.Text;
    using System.Windows;
    using System.Windows.Media;
    using GoldMiner.Model.Base;

    /// <summary>
    /// Hook game item class.
    /// </summary>
    public class Hook : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Hook"/> class.
        /// </summary>
        /// <param name="position">position.</param>
        public Hook(Vector2 position)
            : base(position)
        {
            var scale = 2;

            Point p1 = new Point(0 * scale, 0 * scale); // CTRL.
            Point p2 = new Point(0 * scale, 11 * scale);
            Point p3_1 = new Point(-3 * scale, 13 * scale);
            Point p4_1 = new Point(-5 * scale, 13 * scale);
            Point p5_1 = new Point(-6 * scale, 12 * scale);
            Point p6_1 = new Point(-6 * scale, 10 * scale);
            Point p7_1 = new Point(-5 * scale, 12 * scale);

            Point p3_2 = new Point(3 * scale, 13 * scale);
            Point p4_2 = new Point(5 * scale, 13 * scale);
            Point p5_2 = new Point(6 * scale, 12 * scale);
            Point p6_2 = new Point(6 * scale, 10 * scale);
            Point p7_2 = new Point(5 * scale, 12 * scale);

            // Vonalak kellenek a Pontok között (Madár alakzata az a V alakú cucc)
            GeometryGroup g = new GeometryGroup(); // CTRL.
            g.Children.Add(new LineGeometry(p1, p2));
            g.Children.Add(new LineGeometry(p2, p3_1));
            g.Children.Add(new LineGeometry(p3_1, p4_1));
            g.Children.Add(new LineGeometry(p4_1, p5_1));
            g.Children.Add(new LineGeometry(p5_1, p6_1));
            g.Children.Add(new LineGeometry(p6_1, p7_1));

            g.Children.Add(new LineGeometry(p2, p3_2));
            g.Children.Add(new LineGeometry(p3_2, p4_2));
            g.Children.Add(new LineGeometry(p4_2, p5_2));
            g.Children.Add(new LineGeometry(p5_2, p6_2));
            g.Children.Add(new LineGeometry(p6_2, p7_2));

            g.Children.Add(new EllipseGeometry(new Point(0, 0), 2 * scale, 2 * scale));
            this.area = g.GetWidenedPathGeometry(new Pen(Brushes.Black, 2)); // Szeretném használni 2 pixeles vastagságban a vonalat -> ütközésvizsgálat
        }
    }
}
