﻿// <copyright file="Diamond.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model
{
    using System;
    using System.Collections.Generic;
    using System.Numerics;
    using System.Text;
    using GoldMiner.Model.Base;
    using GoldMiner.Model.Helper;

    /// <summary>
    /// Diamond game item class.
    /// </summary>
    public class Diamond : Mineral
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Diamond"/> class.
        /// </summary>
        /// <param name="position">position.</param>
        /// <param name="stoneSizeType">size of stone type.</param>
        public Diamond(Vector2 position, StoneSizeType stoneSizeType)
            : base(position, stoneSizeType)
        {
            switch (stoneSizeType)
            {
                case StoneSizeType.Small:
                    this.BrushName = "GoldMiner.View.Images.diamond.png"; // TODO
                    this.Weight = 4;
                    this.Value = 800;
                    break;
                case StoneSizeType.Medium:
                    this.BrushName = "GoldMiner.View.Images.diamond.png";
                    this.Weight = 4;
                    this.Value = 800;
                    break;
                case StoneSizeType.Large:
                    this.BrushName = "GoldMiner.View.Images.diamond.png"; // TODO
                    break;
                default:
                    break;
            }
        }
    }
}
