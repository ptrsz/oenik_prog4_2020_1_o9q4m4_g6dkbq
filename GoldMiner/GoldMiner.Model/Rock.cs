﻿// <copyright file="Rock.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model
{
    using System;
    using System.Collections.Generic;
    using System.Numerics;
    using System.Text;
    using GoldMiner.Model.Base;
    using GoldMiner.Model.Helper;

    /// <summary>
    /// Rock game item class.
    /// </summary>
    public class Rock : Mineral
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Rock"/> class.
        /// </summary>
        /// <param name="position">position.</param>
        /// <param name="stoneSizeType">stone size type.</param>
        public Rock(Vector2 position, StoneSizeType stoneSizeType)
            : base(position, stoneSizeType)
        {
            switch (stoneSizeType)
            {
                case StoneSizeType.Small:
                    this.BrushName = "GoldMiner.View.Images.rock1.png";
                    this.Weight = 1;
                    this.Value = 1;
                    break;
                case StoneSizeType.Medium:
                    this.BrushName = "GoldMiner.View.Images.rock5.png";
                    this.Weight = 3;
                    this.Value = 3;
                    break;
                case StoneSizeType.Large:
                    this.BrushName = "GoldMiner.View.Images.rock8.png";
                    this.Weight = 5;
                    this.Value = 10;
                    break;
                default:
                    break;
            }
        }
    }
}
