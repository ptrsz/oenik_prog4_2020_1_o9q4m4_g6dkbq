﻿// <copyright file="Gold.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model
{
    using System.Numerics;
    using GoldMiner.Model.Base;
    using GoldMiner.Model.Helper;

    /// <summary>
    /// Gold game item class.
    /// </summary>
    public class Gold : Mineral
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Gold"/> class.
        /// </summary>
        /// <param name="position">position.</param>
        /// <param name="stoneSizeType">size of stone type.</param>
        public Gold(Vector2 position, StoneSizeType stoneSizeType)
            : base(position, stoneSizeType)
        {
            switch (stoneSizeType)
            {
                case StoneSizeType.Small:
                    this.BrushName = "GoldMiner.View.Images.gold1.png";
                    this.Weight = 1;
                    this.Value = 100;
                    break;
                case StoneSizeType.Medium:
                    this.BrushName = "GoldMiner.View.Images.gold5.png";
                    this.Weight = 3;
                    this.Value = 250;
                    break;
                case StoneSizeType.Large:
                    this.BrushName = "GoldMiner.View.Images.gold8.png";
                    this.Weight = 5;
                    this.Value = 500;
                    break;
                default:
                    break;
            }
        }
    }
}
