﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model
{
    using System;

    /// <summary>
    /// Player class.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Score updated event.
        /// </summary>
        public event EventHandler ScoreUpdated;

        /// <summary>
        /// Level changed event.
        /// </summary>
        public event EventHandler LevelChanged;

        /// <summary>
        /// Level limit changed event,.
        /// </summary>
        public event EventHandler LevelLimitChanged;

        /// <summary>
        /// Gets or sets name of player.
        /// </summary>
        public string Name { get; set; }

        private int level;

        public int Level
        {
            get
            {
                return this.level;
            }

            set
            {
                this.level = value;
                this.LevelChanged?.Invoke(this, null);
            }
        }

        private long score;

        public long Score
        {
            get
            {
                return this.score;
            }

            set
            {
                this.score = value;
                this.ScoreUpdated?.Invoke(this, null);
            }
        }

        private long levelLimit;

        public long LevelLimit
        {
            get
            {
                return this.levelLimit;
            }

            set
            {
                this.levelLimit = value;
                this.LevelLimitChanged?.Invoke(this, null);
            }
        }

        public Player()
        {
            this.score = 0;
            this.level = 1;
            this.levelLimit = 1000;
        }

        public DateTime GameStartTime { get; set; }

        public DateTime GameEndTime { get; set; }
    }
}
