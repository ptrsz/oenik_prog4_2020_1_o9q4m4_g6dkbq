﻿// <copyright file="GameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model.Base
{
    using System;
    using System.Numerics;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Game item class.
    /// </summary>
    public class GameItem
    {
        public Vector2 InitPosition = Vector2.Zero;

        public bool IsEnabled { get; set; } = true;

        public Vector2 Position = Vector2.Zero;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameItem"/> class.
        /// </summary>
        /// <param name="position">position</param>
        public GameItem(Vector2 position)
        {
            this.Position = position;
            this.InitPosition = position;
        }

        public Point PositionAsPoint
        {
            get { return new Point(this.Position.X, this.Position.Y); }
        }

        // Ebben tároljuk el az alakzatot
        protected Geometry area = new GeometryGroup(); // CTRL. // (0;0) centered

        // Forgatási szög FOKban
        public double RotDegree;

        // Középpontra is szükség van:
        // CX, CY = középpont koordinátája! (nem bal felső sarok)
        // public double CX { get; set; }

        // public double CY { get; set; }

        // Getter: Valaki radiánt olvas -> fokból radiánt konvertál
        // Set: valaki radiánt próbál írni -> radiánból fokot konvertál
        public double Rad
        {
            get
            {
                return Math.PI * this.RotDegree / 180;
            }

            set
            {
                this.RotDegree = 180 * value / Math.PI;
            }
        }

        /// <summary>
        /// Gets class for RealArea. Forgatás számításához szükséges.
        /// </summary>
        public Geometry RealArea
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(this.Position.X, this.Position.Y)); // ELTOLOM AZ ALAKZATOT CX CY-ba
                tg.Children.Add(new RotateTransform(this.RotDegree, this.Position.X, this.Position.Y)); // Elforgatom az alakzatot CX CY körül rotdegree fokkal
                this.area.Transform = tg;

                return this.area.GetFlattenedPathGeometry(); // Ez adja vissza a transzformáció eredményét
            }
        }

        /// <summary>
        /// Ütközésvizsgálat -> Egyik GameItem ütközik-e másik GameItemmel.
        /// </summary>
        /// <param name="other">other.</param>
        /// <returns>bool</returns>
        public bool IsCollision(GameItem other)
        {
            if (other is Diamond)
            {
                ;
            }

            // Metszet geometriát határozza meg => ha nagyobb mint 0 akkor van ütközés -> 2 GameItemnek van metszete azaz ütköztek!
            return Geometry.Combine(this.RealArea, other.RealArea, GeometryCombineMode.Intersect, null).GetArea() > 0 && other.IsEnabled;
        }
    }
}
