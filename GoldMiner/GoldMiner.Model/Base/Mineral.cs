﻿// <copyright file="Mineral.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Model.Base
{
    using System.Numerics;
    using System.Windows;
    using System.Windows.Media;
    using GoldMiner.Model.Helper;

    public class Mineral : GameItem
    {
        private readonly float smallStoneSize = 20;
        private readonly float mediumStoneSize = 40;
        private readonly float largeStoneSize = 60;

        public float Weight = 1;
        public long Value = 0;

        public string BrushName;

        public Mineral(Vector2 position, StoneSizeType stoneSizeType)
            : base(position)
        {
            this.Position.X = position.X;
            this.Position.Y = position.Y;

            this.InitGeometry(stoneSizeType);
        }

        public virtual void InitGeometry(StoneSizeType stoneSizeType)
        {
            GeometryGroup g = new GeometryGroup();

            float currentSize = 0.0f;

            switch (stoneSizeType)
            {
                case StoneSizeType.Small:
                    currentSize = this.smallStoneSize;
                    break;
                case StoneSizeType.Medium:
                    currentSize = this.mediumStoneSize;
                    break;
                case StoneSizeType.Large:
                    currentSize = this.largeStoneSize;
                    break;
                default:
                    break;
            }

            g.Children.Add(new RectangleGeometry(new Rect(0, 0, currentSize, currentSize)));
            this.area = g;
        }
    }
}
