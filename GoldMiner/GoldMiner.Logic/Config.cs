﻿// <copyright file="Config.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Logic
{
    using System.Drawing;

    public static class Config
    {
        public static double Width = 1000;
        public static double Height = 600;

        public static float UpgroundHeight = 60;
        public static float UndergroundHeight = 540;

        public static Brush UpgroundBrush;
        public static Brush UndergroundBrush;
    }
}
