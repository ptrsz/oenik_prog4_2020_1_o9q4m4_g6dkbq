﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Numerics;
    using System.Reflection;
    using System.Windows;
    using GoldMiner.Model;
    using GoldMiner.Model.Base;
    using GoldMiner.Model.Helper;

    /// <summary>
    /// Logic class for game logic.
    /// </summary>
    public class Logic
    {
        private Player currentPlayer;
        private Model model;

        public event EventHandler RefreshScreen;

        private float hookRotationSpeed = 5.0f;
        private float hookRotationBoundaries = 80.0f;
        public bool IsHookRotatingClockwise = true;
        public bool ShouldHookRotate = true;
        public bool IsHookShooted = false;
        public bool IsHookPulled = false;

        // Ez kell ahhoz hogy a különböző kövek lassabban jöjjenek fel
        private float defaultHookShootSpeed = 15.0f;
        private float hookShootSpeed = 15.0f;
        private float maxHookDistance = 600.0f;
        private Mineral gameItem;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fname"></param>
        /// <param name="currentPlayer"></param>
        public Logic(Model model, string fname, Player currentPlayer)
        {
            this.currentPlayer = currentPlayer;
            this.model = model;

            this.InitModel(fname);
        }

        /// <summary>
        /// Moves the hook.
        /// </summary>
        public void MoveHook()
        {
            if (this.IsHookRotatingClockwise)
            {
                this.model.Hook.RotDegree -= this.hookRotationSpeed;
                if (this.model.Hook.RotDegree <= -this.hookRotationBoundaries)
                {
                    this.IsHookRotatingClockwise = false;
                }
            }
            else
            {
                this.model.Hook.RotDegree += this.hookRotationSpeed;

                if (this.model.Hook.RotDegree >= this.hookRotationBoundaries)
                {
                    this.IsHookRotatingClockwise = true;
                }
            }

            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Moves gold objects.
        /// </summary>
        public void MoveGolds()
        {
            for (int i = 0; i < this.model.GoldList.Count; i++)
            {
                this.model.GoldList[i].Position.Y += 10;
            }

            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Pulls the hook.
        /// </summary>
        public void PullHook()
        {
            if (this.model.Hook.RotDegree < 0)
            {
                this.model.Hook.Position.X -= (float)Math.Cos((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
                this.model.Hook.Position.Y -= (float)Math.Sin((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
            }
            else
            {
                this.model.Hook.Position.X -= (float)-Math.Cos((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
                this.model.Hook.Position.Y -= (float)Math.Sin((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
            }

            if (this.gameItem != null)
            {
                if (this.model.Hook.RotDegree < 0)
                {
                    this.gameItem.Position.X -= (float)Math.Cos((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
                    this.gameItem.Position.Y -= (float)Math.Sin((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
                }
                else
                {
                    this.gameItem.Position.X -= (float)-Math.Cos((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
                    this.gameItem.Position.Y -= (float)Math.Sin((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
                }
            }

            var containerRect = new Rect(this.model.Hook.InitPosition.X - this.maxHookDistance, this.model.Hook.InitPosition.Y, 2 * this.maxHookDistance, this.maxHookDistance);

            // if (model.Hook.Position.X<=model.Hook.InitPosition.X-maxHookDistance || model.Hook.Position.Y <= model.Hook.InitPosition.Y)
            if (!containerRect.Contains(this.model.Hook.PositionAsPoint))
            {
                this.IsHookPulled = false;
                this.ShouldHookRotate = true;
                if (this.gameItem != null)
                {
                    this.hookShootSpeed = this.defaultHookShootSpeed;
                    this.currentPlayer.Score += this.gameItem.Value;
                    this.gameItem.IsEnabled = false;
                    this.gameItem = null;

                    // level change if score reached
                    if (this.currentPlayer.Score > this.currentPlayer.LevelLimit)
                    {
                        this.currentPlayer.Level += 1;
                        this.SwitchModelToLevel(this.currentPlayer.Level);
                        this.currentPlayer.GameStartTime = DateTime.Now;
                    }
                }
            }

            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Shoots the hook.
        /// </summary>
        public void ShootHook()
        {
            this.IsHookShooted = true;
            var aimDirection = this.model.Hook.RotDegree;

            // Shoot the hook
            // azért kell hogy mindkét irányba kilőhető legyen ( - , + )
            if (this.model.Hook.RotDegree < 0)
            {
                this.model.Hook.Position.X += (float)Math.Cos((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
                this.model.Hook.Position.Y += (float)Math.Sin((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
            }
            else
            {
                this.model.Hook.Position.X += (float)-Math.Cos((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
                this.model.Hook.Position.Y += (float)Math.Sin((Math.PI / 2) - Math.Abs(this.model.Hook.Rad)) * this.hookShootSpeed;
            }

            // Collision detection
            foreach (var mineral in this.model.Minerals)
            {
                if (this.model.Hook.IsCollision(mineral))
                {
                    this.hookShootSpeed = this.defaultHookShootSpeed * (1 / mineral.Weight);
                    this.gameItem = mineral;
                    this.IsHookShooted = false;
                    this.IsHookPulled = true;
                }
            }

            // Pull back the hook if max length is reached
            if (Vector2.Distance(this.model.Hook.InitPosition, this.model.Hook.Position) >= this.maxHookDistance)
            {
                this.IsHookShooted = false;
                this.IsHookPulled = true;
            }

            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        private void InitModel(string fname)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
            StreamReader sr = new StreamReader(stream);
            string[] lines = sr.ReadToEnd().Replace("\r", "").Split('\n');

            this.model.TileSize = new Size(20, 20); // 20px = 1 Tile

            // todo: a width et es a height ot configbol kell majd olvasni
            int tileWidth = (int)Math.Round(Config.Width / this.model.TileSize.Width);
            int tileHeight = (int)Math.Round(Config.UndergroundHeight / this.model.TileSize.Height);

            // tile modban egyebkent perpill width = 35 height = 25 tile
            int upgroundTileHeight = (int)(Config.UpgroundHeight / this.model.TileSize.Height);

            // map beolvasasa, x es y tile koordinatak
            // a teljes jatek 700*500, de az underground resz csak 700*440 px vagy 35*22 tile
            for (int y = 0; y < tileHeight; y++)
            {
                for (int x = 0; x < tileWidth; x++)
                {
                    Vector2 currentItemPosition =  currentItemPosition = new Vector2((float)(x * this.model.TileSize.Width), (float)((y * this.model.TileSize.Height) + Config.UpgroundHeight));

                    char current = lines[y][x];
                    switch (current)
                    {
                        case 'o':
                            // ures
                            break;
                        case 'x':
                            // small gold
                            this.model.GoldList.Add(new Gold(currentItemPosition, StoneSizeType.Small));
                            break;
                        case 'y':
                            // medium gold
                            this.model.GoldList.Add(new Gold(currentItemPosition, StoneSizeType.Medium));
                            break;
                        case 'z':
                            // medium gold:
                            this.model.GoldList.Add(new Gold(currentItemPosition, StoneSizeType.Large));
                            break;
                        case 'r':
                            // small rock
                            this.model.RockList.Add(new Rock(currentItemPosition, StoneSizeType.Small));
                            break;
                        case 'a':
                            // medium rock
                            this.model.RockList.Add(new Rock(currentItemPosition, StoneSizeType.Medium));
                            break;
                        case 'b':
                            this.model.RockList.Add(new Rock(currentItemPosition, StoneSizeType.Large));
                            break;
                        case 'd':
                            // diamond
                            this.model.DiamondList.Add(new Diamond(currentItemPosition, StoneSizeType.Small));
                            break;
                        case 'e':
                            // oildrums
                            this.model.OilList.Add(new Oil(currentItemPosition, StoneSizeType.Medium));
                            break;
                        case 'f':
                            // plastic bottles
                            this.model.PlasticList.Add(new Plastic(currentItemPosition, StoneSizeType.Medium));
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Clears the model.
        /// </summary>
        public void ClearModel()
        {
            this.model.GoldList = new List<Gold>();
            this.model.OilList = new List<Oil>();
            this.model.PlasticList = new List<Plastic>();
            this.model.RockList = new List<Rock>();
            this.model.DiamondList = new List<Diamond>();
        }

        private void SwitchModelToLevel(int level)
        {
            this.ClearModel();

            switch (level)
            {
                case 1:
                    this.InitModel("GoldMiner.Logic.Levels.one.lvl");
                    this.currentPlayer.LevelLimit = 1000;
                    break;
                case 2:
                    this.InitModel("GoldMiner.Logic.Levels.two.lvl");
                    this.currentPlayer.LevelLimit = 2300;
                    break;
                case 3:
                    this.InitModel("GoldMiner.Logic.Levels.three.lvl");
                    this.currentPlayer.LevelLimit = 8000;
                    break;
                default:
                    this.InitModel("GoldMiner.Logic.Levels.one.lvl");
                    this.currentPlayer.LevelLimit = 1000;
                    break;
            }

            this.currentPlayer.Level = level;
        }
    }
}
