﻿// <copyright file="ViewModelLocator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View
{
    using System;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Views;
    using GoldMiner.Repository;
    using GoldMiner.View.Helper;
    using GoldMiner.View.ViewModels;
    using Microsoft.Practices.ServiceLocation;

    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            this.SetupNavigation();

            SimpleIoc.Default.Register<HomePageViewModel>();
            SimpleIoc.Default.Register<GamePageViewModel>();
            SimpleIoc.Default.Register<HelpPageViewModel>();
            SimpleIoc.Default.Register<MainWindowViewModel>();
            SimpleIoc.Default.Register<LeaderboardViewModel>();
            SimpleIoc.Default.Register<UserFormPageViewModel>();
            SimpleIoc.Default.Register<FinishPageViewModel>();

            this.RegisterPlayerRepository();
            this.RegisterPermanentStorageRepository();
        }

        private void RegisterPermanentStorageRepository()
        {
            if (SimpleIoc.Default.IsRegistered<IPermanentStorageRepository>())
            {
                return;
            }

            var permanentStorageRepository = new PermanentStorageRepository();

            SimpleIoc.Default.Register<IPermanentStorageRepository>(() => permanentStorageRepository);
        }

        private void RegisterPlayerRepository()
        {
            if (SimpleIoc.Default.IsRegistered<IPlayerRepository>())
            {
                return;
            }

            var userRepository = new PlayerRepository();

            SimpleIoc.Default.Register<IPlayerRepository>(() => userRepository);
        }

        private void SetupNavigation()
        {
            if (SimpleIoc.Default.IsRegistered<IFrameNavigationService>())
            {
                return;
            }

            var navigationService = new FrameNavigationService();

            navigationService.Configure("GamePage", new Uri("Views/GamePage.xaml", UriKind.Relative));
            navigationService.Configure("HelpPage", new Uri("Views/HelpPage.xaml", UriKind.Relative));
            navigationService.Configure("HomePage", new Uri("Views/HomePage.xaml", UriKind.Relative));
            navigationService.Configure("UserFormPage", new Uri("Views/UserFormPage.xaml", UriKind.Relative));
            navigationService.Configure("FinishPage", new Uri("Views/FinishPage.xaml", UriKind.Relative));
            navigationService.Configure("LeaderboardPage", new Uri("Views/LeaderboardPage.xaml", UriKind.Relative));

            navigationService.Configure("MainWindow", new Uri("MainWindow.xaml", UriKind.Relative));

            SimpleIoc.Default.Register<INavigationService>(() => navigationService);
        }

        public FinishPageViewModel FinishPageInstance
        {
            get { return ServiceLocator.Current.GetInstance<FinishPageViewModel>(); }
        }

        public UserFormPageViewModel UserFormPageInstance
        {
            get { return ServiceLocator.Current.GetInstance<UserFormPageViewModel>(); }
        }

        public LeaderboardViewModel LeaderboardInstance
        {
            get { return ServiceLocator.Current.GetInstance<LeaderboardViewModel>(); }
        }

        public MainWindowViewModel MainWindowInstance
        {
            get { return ServiceLocator.Current.GetInstance<MainWindowViewModel>(); }
        }

        public HomePageViewModel HomePageInstance
        {
            get { return ServiceLocator.Current.GetInstance<HomePageViewModel>(); }
        }

        public GamePageViewModel GamePageInstance
        {
            get { return ServiceLocator.Current.GetInstance<GamePageViewModel>(); }
        }

        public HelpPageViewModel HelpPageInstance
        {
            get { return ServiceLocator.Current.GetInstance<HelpPageViewModel>(); }
        }
    }
}
