﻿// <copyright file="GameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View.Renderer
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Ioc;
    using GoldMiner.Model;
    using GoldMiner.Repository;

    /// <summary>
    /// Game control class.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private Model model;
        private Logic.Logic logic;
        private GameRenderer renderer;
        private DispatcherTimer tickTimer;
        private Player currentPlayer;
        private IPlayerRepository playerRepository;
        private bool isDead = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            this.playerRepository = SimpleIoc.Default.GetInstance<IPlayerRepository>();
            this.currentPlayer = this.playerRepository.CurrentPlayer;

            // TODO
            this.Loaded += this.GMControl_Loaded;
            this.Unloaded += this.Kaka;

            // var aa = ServiceLocator.Current.GetInstance<PlayerRepository>();
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.isDead)
            {
                return;
            }

            if (this.renderer != null)
            {
                drawingContext.DrawDrawing(this.renderer.BuildDrawing());
            }
        }

        private void Kaka(object sender, RoutedEventArgs e)
        {
            this.Loaded -= this.GMControl_Loaded;
            this.isDead = true;
        }

        private void GMControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.isDead)
            {
                return;
            }

            this.currentPlayer = this.playerRepository.CurrentPlayer;
            this.currentPlayer.Level = 1;
            this.model = new Model();
            this.logic = new Logic.Logic(this.model, "GoldMiner.Logic.Levels.one.lvl", this.currentPlayer);
            this.renderer = new GameRenderer(this.model);

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                this.tickTimer = new DispatcherTimer();
                this.tickTimer.Interval = TimeSpan.FromMilliseconds(25);
                this.tickTimer.Tick += this.Timer_Tick;
                this.tickTimer.Start();

                win.KeyDown += this.Win_KeyDown;
            }

            this.logic.RefreshScreen += (obj, args) => this.InvalidateVisual();
            this.InvalidateVisual();
        }

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.isDead)
            {
                return;
            }

            switch (e.Key)
            {
                case Key.Space:
                    // Ez arra kell hogy spacet ne lehessen nyomni húzás közben
                    if (!this.logic.IsHookPulled && !this.logic.IsHookShooted)
                    {
                        this.logic.ShouldHookRotate = false;
                        this.logic.IsHookShooted = true;
                    }

                    break;
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.isDead)
            {
                return;
            }

            if (this.logic.ShouldHookRotate)
            {
                this.logic.MoveHook();
            }
            else if (this.logic.IsHookShooted)
            {
                this.logic.ShootHook();
            }
            else if (this.logic.IsHookPulled)
            {
                this.logic.PullHook();
            }

            // logic.MoveGolds();
        }
    }
}
