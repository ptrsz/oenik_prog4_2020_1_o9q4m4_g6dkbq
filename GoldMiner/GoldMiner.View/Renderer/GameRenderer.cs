﻿// <copyright file="GameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View.Renderer
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using GoldMiner.Logic;
    using GoldMiner.Model;

    /// <summary>
    /// Game renderer class.
    /// </summary>
    internal class GameRenderer
    {
        private Model model;
        private Dictionary<string, Brush> brushes = new Dictionary<string, Brush>();

        // cachelt drawingok gyorsitas miatt
        private Drawing oldUpground;
        private Drawing oldUnderground;
        private Drawing oldPlayer;

        // a cahcelt map cachlet reszei
        // goldok, diamondok, kovek stb GameItem enkent
        private DrawingGroup oldGolds;
        private DrawingGroup oldMinerals;
        private Drawing oldDiamonds;
        private Drawing oldRocks;
        private Drawing oldOils;
        private Drawing oldPlastics;


        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="model">model.</param>
        public GameRenderer(Model model)
        {
            this.model = model;
        }

        /// <summary>
        /// Gets image brush.
        /// </summary>
        public ImageBrush ImageBrush { get; private set; }

        private Brush MinerRestBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.miner_rest.png", false); }
        }

        private Brush MinerPullBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.miner_pull.png", false); }
        }

        private Brush UndergroundBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.underground1.png", false); }
        }

        private Brush UpgroundBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.upground1.png", false); }
        }

        private Brush SmallGoldBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.gold1.png", true); }
        }

        private Brush MediumGoldBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.gold5.png", false); }
        }

        private Brush LargeGoldBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.gold8.png", false); }
        }

        private Brush SmallRockBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.rock1.png", true); }
        }

        private Brush MediumRockBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.rock5.png", false); }
        }

        private Brush LargeRockBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.rock8.png", false); }
        }

        private Brush DiamondBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.diamond.png", true); }
        }

        private Brush HookBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.hook.png", false); }
        }

        private Brush OilBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.oil.png", false); }
        }

        private Brush PlasticBrush
        {
            get { return this.GetBrush("GoldMiner.View.Images.plastic.png", true); }
        }

        /// <summary>
        /// Resets render.
        /// </summary>
        public void Reset()
        {
            this.oldPlayer = null;
            this.oldUnderground = null;
            this.oldUpground = null;
            this.oldGolds = null;
            this.oldRocks = null;
            this.oldDiamonds = null;

            this.brushes.Clear();
        }

        /// <summary>
        /// Draws the render.
        /// </summary>
        /// <returns>drawing group.</returns>
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();

            // mindenfele objektumok hozzaadasa a dg hez, amiket ki kell rajzolni
            dg.Children.Add(this.GetUnderground());
            dg.Children.Add(this.GetUpground());
            dg.Children.Add(this.GetPlayer());
            dg.Children.Add(this.GetHook());
            dg.Children.Add(this.GetChain());

            // dg.Children.Add(GetRocks());
            // dg.Children.Add(GetDiamonds());
            // dg.Children.Add(GetOils());
            // dg.Children.Add(GetPlastics());
            // dg.Children.Add(GetGolds());
            dg.Children.Add(this.GetMineral<Rock>());
            dg.Children.Add(this.GetMineral<Diamond>());
            dg.Children.Add(this.GetMineral<Oil>());
            dg.Children.Add(this.GetMineral<Plastic>());
            dg.Children.Add(this.GetMineral<Gold>());

            return dg;
        }

        private Brush GetBrush(string fname, bool isTiled)
        {
            if (!this.brushes.ContainsKey(fname))
            {
                BitmapImage img = new BitmapImage();
                img.BeginInit();
                img.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
                img.EndInit();

                ImageBrush ib = new ImageBrush(img);

                if (isTiled)
                {
                    ib.TileMode = TileMode.Tile;
                    ib.Viewport = new Rect(0, 0, this.model.TileSize.Width, this.model.TileSize.Height);
                    ib.ViewportUnits = BrushMappingMode.Absolute;
                }

                this.brushes.Add(fname, ib);
            }

            return this.brushes[fname];
        }

        private Drawing GetPlayer()
        {
            if (this.oldPlayer == null)
            {
                // a modelbe majd be kell rakni a playert, de egyelore hard codolva van a helye
                Geometry geometry = new RectangleGeometry(new Rect(this.model.PlayerPos.X * this.model.TileSize.Width, this.model.PlayerPos.Y * this.model.TileSize.Height, this.model.TileSize.Width * 3, this.model.TileSize.Height * 3));

                // Geometry geometry = new RectangleGeometry(new Rect(200,50,60,58));
                this.oldPlayer = new GeometryDrawing(this.MinerRestBrush, null, geometry);
            }

            return this.oldPlayer;
        }

        private Drawing GetUpground()
        {
            if (this.oldUpground == null)
            {
                return new GeometryDrawing(
                    this.UpgroundBrush,
                    new Pen(Brushes.Black, 1),
                    new RectangleGeometry(new System.Windows.Rect(0, 0, Config.Width, Config.UpgroundHeight)));
            }
            else
            {
                return this.oldUpground;
            }
        }

        private Drawing GetUnderground()
        {
            if (this.oldUnderground == null)
            {
                return new GeometryDrawing(
                    this.UndergroundBrush,
                    new Pen(Brushes.Black, 1),
                    new RectangleGeometry(new System.Windows.Rect(0, Config.Height - Config.UndergroundHeight, Config.Width, Config.UndergroundHeight)));
            }
            else
            {
                return this.oldUnderground;
            }
        }

        private DrawingGroup GetMineral<T>()
        {
            if (this.oldMinerals == null)
            {
                DrawingGroup oldMinerals = new DrawingGroup();
                foreach (var mineral in this.model.Minerals.Where(x => x.IsEnabled))
                {
                    GeometryDrawing gd = new GeometryDrawing(this.GetBrush(mineral.BrushName, false), null, mineral.RealArea);
                    oldMinerals.Children.Add(gd);
                }

                return oldMinerals;
            }
            else
            {
                return this.oldMinerals;
            }
        }

        // private DrawingGroup GetGolds()
        // {
        //    if (oldGolds == null)
        //    {
        //        DrawingGroup oldGolds = new DrawingGroup();
        //        foreach (var gold in model.GoldList.Where(x => x.IsEnabled))
        //        {
        //            GeometryDrawing gd = new GeometryDrawing(GetBrush(gold.brushName, false), null, gold.RealArea);
        //            oldGolds.Children.Add(gd);
        //        }

        // return oldGolds;
        //    }
        //    else return oldGolds;
        // }

        // private Drawing GetRocks()
        // {
        //    if (oldRocks == null)
        //    {
        //        GeometryGroup gg = new GeometryGroup();
        //        for (int i = 0; i < model.RockList.Count; i++)
        //        {
        //            Geometry geometry = new RectangleGeometry(new Rect(model.RockList[i].PositionAsPoint, model.TileSize));
        //            gg.Children.Add(geometry);
        //        }
        //        oldRocks = new GeometryDrawing(SmallRockBrush, null, gg);
        //    }
        //    return oldRocks;
        // }

        // private Drawing GetDiamonds()
        // {
        //    if (oldDiamonds == null)
        //    {
        //        GeometryGroup gg = new GeometryGroup();
        //        for (int i = 0; i < model.DiamondList.Count; i++)
        //        {
        //            Geometry geometry = new RectangleGeometry(new Rect(model.DiamondList[i].PositionAsPoint, model.TileSize));
        //            gg.Children.Add(geometry);
        //        }
        //        oldDiamonds = new GeometryDrawing(DiamondBrush, null, gg);
        //    }
        //    return oldDiamonds;
        // }

        // private Drawing GetOils()
        // {
        //    if (oldOils == null)
        //    {
        //        GeometryGroup gg = new GeometryGroup();
        //        for (int i = 0; i < model.OilList.Count; i++)
        //        {
        //            Geometry geometry = new RectangleGeometry(new Rect(model.OilList[i].Position.X, model.OilList[i].Position.Y, model.TileSize.Width, model.TileSize.Height * 1.5f));
        //            gg.Children.Add(geometry);
        //        }
        //        oldOils = new GeometryDrawing(OilBrush, null, gg);
        //    }
        //    return oldOils;
        // }

        // private Drawing GetPlastics()
        // {
        //    if (oldPlastics == null)
        //    {
        //        GeometryGroup gg = new GeometryGroup();
        //        for (int i = 0; i < model.PlasticList.Count; i++)
        //        {
        //            Geometry geometry = new RectangleGeometry(new Rect(model.PlasticList[i].PositionAsPoint, model.TileSize));
        //            gg.Children.Add(geometry);
        //        }
        //        oldPlastics = new GeometryDrawing(PlasticBrush, null, gg);
        //    }
        //    return oldPlastics;
        // }

        // itt nincs oldHook, mert a kampo mindig mozog
        private Drawing GetHook()
        {
            GeometryDrawing gd = new GeometryDrawing(Brushes.Red, null, this.model.Hook.RealArea);
            return gd;
        }

        private Drawing GetChain()
        {
            LineGeometry line = new LineGeometry(new Point(this.model.Hook.InitPosition.X, this.model.Hook.InitPosition.Y), new Point(this.model.Hook.Position.X, this.model.Hook.Position.Y));
            GeometryDrawing gd = new GeometryDrawing(Brushes.DimGray, new Pen(Brushes.DimGray, 3), line);
            return gd;
        }
    }
}
