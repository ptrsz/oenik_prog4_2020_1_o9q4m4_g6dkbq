﻿// <copyright file="GamePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View.Views
{
    using System;
    using System.Windows.Controls;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Ioc;
    using GoldMiner.Model;
    using GoldMiner.Repository;
    using GoldMiner.View.ViewModels;

    public sealed partial class GamePage : Page
    {
        private IPlayerRepository playerRepository;
        private GamePageViewModel gamePageViewModel;
        private Player currentPlayer;
        private TimeSpan gameTime = new TimeSpan(0, 0, 40);
        private DispatcherTimer timer;

        private void timer_Tick(object sender, EventArgs e)
        {
            if (this.currentPlayer != null)
            {
                var remainingTime = (this.currentPlayer.GameStartTime.Add(this.gameTime) - DateTime.Now);
                if (remainingTime <= TimeSpan.Zero)
                {
                    this.currentPlayer.GameEndTime = DateTime.Now;
                    this.timer.Tick -= this.timer_Tick;
                    this.gamePageViewModel.NavigateToFinishPage();
                }

                this.timerLabel.Content = remainingTime.ToString("mm':'ss':'fff"); ;
            }
        }

        public GamePage()
        {
            this.playerRepository = SimpleIoc.Default.GetInstance<IPlayerRepository>();
            this.gamePageViewModel = SimpleIoc.Default.GetInstance<GamePageViewModel>();

            this.currentPlayer = this.playerRepository.CurrentPlayer;
            this.InitializeComponent();
            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(10);
            this.timer.Tick += this.timer_Tick;
            this.timer.Start();
        }

        private void GameControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
        }
    }
}
