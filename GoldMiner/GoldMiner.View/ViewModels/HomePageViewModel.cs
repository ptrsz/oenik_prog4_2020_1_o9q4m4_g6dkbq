﻿// <copyright file="HomePageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View.ViewModels
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;

    public class HomePageViewModel : ViewModelBase
    {
        private INavigationService navigationService;

        public ICommand NewGameCommand => new RelayCommand(() =>
        {
            this.navigationService.NavigateTo("UserFormPage");
        });

        public ICommand HelpCommand => new RelayCommand(() =>
        {
            this.navigationService.NavigateTo("HelpPage");
        });

        public ICommand LeaderboardCommand => new RelayCommand(() =>
        {
            this.navigationService.NavigateTo("LeaderboardPage");
        });

        public ICommand ExitCommand => new RelayCommand(() =>
        {
            System.Windows.Application.Current.Shutdown();
        });

        public HomePageViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
        }
    }
}
