﻿// <copyright file="MainWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View.ViewModels
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;

    public class MainWindowViewModel : ViewModelBase
    {
        private INavigationService navigationService;

        public ICommand LoadHomePage => new RelayCommand(() =>
        {
            this.navigationService.NavigateTo("HomePage");
        });

        public MainWindowViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
        }
    }
}
