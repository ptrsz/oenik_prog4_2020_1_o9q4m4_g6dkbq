﻿// <copyright file="FinishPageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;
    using GoldMiner.Model;
    using GoldMiner.Repository;

    public class FinishPageViewModel : ViewModelBase
    {
        private IPermanentStorageRepository permanentStorageRepository;
        private IPlayerRepository playerRepository;
        private INavigationService navigationService;

        public Player CurrentPlayer { get; set; }

        public ICommand PageLoadedCommand => new RelayCommand(() =>
        {
            this.CurrentPlayer = this.playerRepository.CurrentPlayer;
            this.RaisePropertyChanged(nameof(this.CurrentPlayer));
            this.permanentStorageRepository.SaveLeaderboard();
        });

        public ICommand BackCommand => new RelayCommand(() =>
        {
            this.navigationService.NavigateTo("HomePage");
        });

        public FinishPageViewModel(INavigationService navigationService, IPlayerRepository playerRepository, IPermanentStorageRepository permanentStorageRepository)
        {
            this.permanentStorageRepository = permanentStorageRepository;
            this.playerRepository = playerRepository;
            this.navigationService = navigationService;
        }
    }
}
