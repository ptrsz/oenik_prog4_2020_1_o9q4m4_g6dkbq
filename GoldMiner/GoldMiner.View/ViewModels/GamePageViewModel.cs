﻿// <copyright file="GamePageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View.ViewModels
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;
    using GoldMiner.Repository;

    public class GamePageViewModel : ViewModelBase
    {
        private IPlayerRepository playerRepository;
        private INavigationService navigationService;

        public string UserScore { get; set; }

        public string UserLevel { get; set; }

        public string LevelLimit { get; set; }

        public ICommand PageLoadedCommand => new RelayCommand(() =>
        {
            this.UserScore = this.playerRepository.CurrentPlayer.Score.ToString();
            this.UserLevel = this.playerRepository.CurrentPlayer.Level.ToString();
            this.LevelLimit = this.UserLevel = this.playerRepository.CurrentPlayer.LevelLimit.ToString();

            this.playerRepository.CurrentPlayer.ScoreUpdated += this.CurrentPlayer_ScoreUpdated;
            this.playerRepository.CurrentPlayer.LevelChanged += this.CurrentPlayer_LevelChanged;
            this.playerRepository.CurrentPlayer.LevelLimitChanged += this.CurrentPlayer_LevelLimitChanged;

            if (!this.playerRepository.PlayerList.Contains(this.playerRepository.CurrentPlayer))
            {
                this.playerRepository.PlayerList.Add(this.playerRepository.CurrentPlayer);
            }

            this.RaisePropertyChanged(nameof(this.UserScore));
            this.RaisePropertyChanged(nameof(this.UserLevel));
            this.RaisePropertyChanged(nameof(this.LevelLimit));
        });

        public void NavigateToFinishPage()
        {
            this.navigationService.NavigateTo("FinishPage");
        }

        public ICommand BackCommand => new RelayCommand(() =>
        {
            this.navigationService.NavigateTo("HomePage");
        });

        public GamePageViewModel(INavigationService navigationService, IPlayerRepository playerRepository)
        {
            this.playerRepository = playerRepository;
            this.navigationService = navigationService;

            // TBD
        }

        private void CurrentPlayer_ScoreUpdated(object sender, System.EventArgs e)
        {
            this.UserScore = this.playerRepository.CurrentPlayer.Score.ToString();
            this.RaisePropertyChanged(nameof(this.UserScore));
        }

        private void CurrentPlayer_LevelChanged(object sender, System.EventArgs e)
        {
            this.UserLevel = "Level " + this.playerRepository.CurrentPlayer.Level.ToString();
            this.RaisePropertyChanged(nameof(this.UserLevel));
        }

        private void CurrentPlayer_LevelLimitChanged(object sender, System.EventArgs e)
        {
            this.LevelLimit = this.playerRepository.CurrentPlayer.LevelLimit.ToString();
            this.RaisePropertyChanged(nameof(this.LevelLimit));
        }
    }
}
