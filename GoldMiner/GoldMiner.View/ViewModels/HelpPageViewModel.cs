﻿// <copyright file="HelpPageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View.ViewModels
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;

    public class HelpPageViewModel : ViewModelBase
    {
        private INavigationService navigationService;

        public ICommand BackCommand => new RelayCommand(() =>
        {
            this.navigationService.GoBack();
        });

        public HelpPageViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
        }
    }
}
