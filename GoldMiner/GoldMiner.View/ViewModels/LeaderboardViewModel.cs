﻿// <copyright file="LeaderboardViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View.ViewModels
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;
    using GoldMiner.Model;
    using GoldMiner.Repository;

    public class LeaderboardViewModel : ViewModelBase
    {
        public ObservableCollection<Player> PlayerList { get; set; } = new ObservableCollection<Player>();

        private IPlayerRepository playerRepository;
        private INavigationService navigationService;
        private IPermanentStorageRepository permanentStorageRepository;

        public ICommand RefreshPageContentCommand => new RelayCommand(() =>
        {
            this.permanentStorageRepository.LoadLeaderboard();

            this.PlayerList.Clear();
            this.RaisePropertyChanged(nameof(this.PlayerList));

            foreach (var player in this.playerRepository.PlayerList.OrderByDescending(x => x.Score))
            {
                this.PlayerList.Add(player);
            }

            this.RaisePropertyChanged(nameof(this.PlayerList));
        });

        public ICommand BackCommand => new RelayCommand(() =>
        {
            this.navigationService.GoBack();
        });

        public LeaderboardViewModel(INavigationService navigationService, IPlayerRepository playerRepository, IPermanentStorageRepository permanentStorageRepository)
        {
            this.permanentStorageRepository = permanentStorageRepository;
            this.playerRepository = playerRepository;
            this.navigationService = navigationService;
        }
    }
}
