﻿// <copyright file="UserFormPageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GoldMiner.View.ViewModels
{
    using System;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;
    using GoldMiner.Model;
    using GoldMiner.Repository;

    public class UserFormPageViewModel : ViewModelBase
    {
        private IPlayerRepository playerRepository;
        private INavigationService navigationService;

        public string UserName { get; set; }

        public ICommand StartGameCommand => new RelayCommand(() =>
        {
            var currentPlayer = new Player()
            {
                Name = string.IsNullOrEmpty(this.UserName) ? "Üres user" : this.UserName,
                GameStartTime = DateTime.Now
            };
            this.playerRepository.CurrentPlayer = null;
            this.playerRepository.CurrentPlayer = currentPlayer;
            this.UserName = string.Empty;

            this.navigationService.NavigateTo("GamePage");
        });

        public ICommand BackCommand => new RelayCommand(() =>
        {
            this.navigationService.GoBack();
        });

        public UserFormPageViewModel(INavigationService navigationService, IPlayerRepository playerRepository)
        {
            this.playerRepository = playerRepository;
            this.navigationService = navigationService;
        }
    }
}
